#Raspberry n°24 serveur 1

@test "connection port 80 raspberry 024" {
	run nmap -p80 piensg024.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 024" {
	run nmap -p3000 piensg024.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°26 serveur 1

@test "connection port 80 raspberry 026" {
	run nmap -p80 piensg026.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 026" {
	run nmap -p3000 piensg026.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°23 serveur 1

@test "connection port 80 raspberry 023" {
	run nmap -p80 piensg023.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 023" {
	run nmap -p3000 piensg023.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°22 serveur 1

@test "connection port 80 raspberry 022" {
	run nmap -p80 piensg022.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 022" {
	run nmap -p3000 piensg022.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°19 serveur 1

@test "connection port 80 raspberry 019" {
	run nmap -p80 piensg019.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 019" {
	run nmap -p3000 piensg019.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}


#======================================================
#Raspberry n°18 serveur 1

@test "connection port 80 raspberry 018" {
	run nmap -p80 piensg018.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 018" {
	run nmap -p3000 piensg018.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°15 serveur 1

@test "connection port 80 raspberry 015" {
	run nmap -p80 piensg015.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 015" {
	run nmap -p3000 piensg015.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°10 serveur 2

@test "connection port 80 raspberry 010" {
	run nmap -p80 piensg010.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 010" {
	run nmap -p3000 piensg010.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°11 serveur 2

@test "connection port 80 raspberry 011" {
	run nmap -p80 piensg011.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 011" {
	run nmap -p3000 piensg011.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°16 serveur 2

@test "connection port 80 raspberry 016" {
	run nmap -p80 piensg016.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 016" {
	run nmap -p3000 piensg016.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°08 serveur 2

@test "connection port 80 raspberry 008" {
	run nmap -p80 piensg008.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 008" {
	run nmap -p3000 piensg008.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°06 serveur 3

@test "connection port 80 raspberry 003" {
	run nmap -p80 piensg003.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 003" {
	run nmap -p3000 piensg003.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°20 serveur 3

@test "connection port 80 raspberry 020" {
	run nmap -p80 piensg020.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}
@test "connection port 3000 raspberry 020" {
	run nmap -p3000 piensg020.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Raspberry n°21 serveur 3

@test "connection port 80 raspberry 021" {
	run nmap -p80 piensg021.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "80/tcp open  http" ]
}

@test "connection port 3000 raspberry 021" {
	run nmap -p3000 piensg021.ensg.eu
	[ $status -eq 0 ]
	[ "${lines[5]}" = "3000/tcp open  ppp" ]
}

#======================================================
#Ordinateur Base de Données hp1706w013

@test "connection port 5432 hp1706w013" {
	run nmap -p5432 hp1706w013
	[ $status -eq 0 ]
	[ "${lines[5]}" = "5432/tcp closed postgresql" ]
}

@test "connection port 8086 hp1706w013" {
	run nmap -p8086 hp1706w013
	[ $status -eq 0 ]
	[ "${lines[5]}" = "8086/tcp open  d-s-n" ]
}

#======================================================
#Ordinateur Base de Données hp1706w014

@test "connection port 5432 hp1706w014" {
	run nmap -p5432 hp1706w014
	[ $status -eq 0 ]
	[ "${lines[5]}" = "5432/tcp closed postgresql" ]
}

@test "connection port 8086 hp1706w014" {
	run nmap -p8086 hp1706w014
	[ $status -eq 0 ]
	[ "${lines[5]}" = "8086/tcp open  d-s-n" ]
}

