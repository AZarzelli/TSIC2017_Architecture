#!/bin/bash
# Test if all test passed then run update process
# If test failed rollback
# $1 => image, $2 => service
bats test_images.bats
if [ "$?" -eq "0" ]; 
then 
  bats test_ports.bats
  if [ "$?" -eq "0" ];
    then
      docker service update --image $1 --update-failure-action "rollback" --update-order start-first --update-parallelism 3 --force $2 
  else echo "yaOmoins1problem"
fi
else
 echo "yaOmoins1problem"
fi


