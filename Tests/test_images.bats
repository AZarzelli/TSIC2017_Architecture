#======================================================
#Image Traefik

#--------------------------------
#HTTPD

@test "image traefik manager 019 httpd" {
	curl -I http://piensg019.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 018 httpd" {
	curl -I http://piensg018.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 010 httpd" {
	curl -I http://piensg010.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 011 httpd" {
	curl -I http://piensg011.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 020 httpd" {
	curl -I http://piensg020.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 021 httpd" {
	curl -I http://piensg021.ensg.eu:80/ | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

#--------------------------------
#NODE

@test "image traefik manager 019 node" {
	curl -I http://piensg019.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 018 node" {
	curl -I http://piensg018.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 010 node" {
	curl -I http://piensg010.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 011 node" {
	curl -I http://piensg011.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 020 node" {
	curl -I http://piensg020.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image traefik manager 021 node" {
	curl -I http://piensg021.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

#======================================================
#Image httpd

#--------------------------------
#Raspberry serveur 1

@test "image httpd rasp 024" {
	curl -I http://piensg024.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg024.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 026" {
	curl -I http://piensg026.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg026.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 023" {
	curl -I http://piensg023.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg023.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 022" {
	curl -I http://piensg022.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg022.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 019" {
	curl -I http://piensg019.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg019.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 018" {
	curl -I http://piensg018.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg018.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 015" {
	curl -I http://piensg015.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg015.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

#--------------------------------
#Raspberry serveur 2

@test "image httpd rasp 010" {
	curl -I http://piensg010.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg010.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 011" {
	curl -I http://piensg011.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg011.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 016" {
	curl -I http://piensg016.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg016.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 008" {
	curl -I http://piensg008.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg008.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

#--------------------------------
#Raspberry serveur 3

@test "image httpd rasp 003" {
	curl -I http://piensg003.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg003.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 020" {
	curl -I http://piensg020.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg020.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

@test "image httpd rasp 021" {
	curl -I http://piensg021.ensg.eu/test.html | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://piensg021.ensg.eu/test.html
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}

#======================================================
#Image Node

@test "test node hp1706w014" {
	curl -I http://hp1706w014.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://hp1706w014.ensg.eu/app/test
	[ $status -eq 0 ]
	#[ "${lines[3]}" = "ok" ]
}

@test "test node hp1706w013" {
	curl -I http://hp1706w013.ensg.eu/app/test | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
	run curl http://hp1706w013.ensg.eu/app/test
	[ $status -eq 0 ]
	[ "${lines[3]}" = "ok" ]
}
#======================================================
#Image Influx

@test "image influx ordinateur hp1706w013" {
	curl  'http://hp1706w013.ensg.eu:3000/chronograf/v1/sources/3/proxy' -H 'Host: hp1706w013.ensg.eu:3000' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Referer: http://hp1706w013.ensg.eu:3000/sources/3/hosts/2437923cd4e9' -H 'Content-Type: application/json;charset=utf-8' -H 'DNT: 1' -H 'Connection: keep-alive' --data $'{"tempVars":[],"query":"SELECT non_negative_derivative(max(\\"bytes_sent\\"), 1s) / 125000 as \\"tx_megabits_per_second\\" FROM \\"net\\" where time > now() - 1h and \\"host\\" = \'2437923cd4e9\' group by time(1m)","resolution":180,"db":"telegraf"}' -w "%{http_code}" -s | {
		run cut -d $'\n' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "image influx ordinateur hp1706w014" {
	curl  'http://hp1706w014.ensg.eu:3000/chronograf/v1/sources/3/proxy' -H 'Host: hp1706w013.ensg.eu:3000' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Referer: http://hp1706w013.ensg.eu:3000/sources/3/hosts/2437923cd4e9' -H 'Content-Type: application/json;charset=utf-8' -H 'DNT: 1' -H 'Connection: keep-alive' --data $'{"tempVars":[],"query":"SELECT non_negative_derivative(max(\\"bytes_sent\\"), 1s) / 125000 as \\"tx_megabits_per_second\\" FROM \\"net\\" where time > now() - 1h and \\"host\\" = \'2437923cd4e9\' group by time(1m)","resolution":180,"db":"telegraf"}' -w "%{http_code}" -s | {
		run cut -d $'\n' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}


#======================================================
#Image Postgres

@test "test posgres hp1706w014" {
	curl -I "http://hp1706w014.ensg.eu/app/route?from=47.81465312656766,2.3808574676513676&to=47.80984013240782,2.3813295364379887&type=distance&avoid=" | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

@test "test posgres hp1706w013" {
	curl -I "http://hp1706w013.ensg.eu/app/route?from=47.81465312656766,2.3808574676513676&to=47.80984013240782,2.3813295364379887&type=distance&avoid=" | head -n 1 |{
		run cut -d $' ' -f2
		[ $output -eq 200 ]
		[ $status -eq 0 ]
	}
}

